!#/bin/bash

## update
sudo pacman -Syu git --noconfirm

## paru insatll (it's a replacement to yay because it's gone the way of the yourt and is no longer maintained)
cd ~/Downloads/setup/
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin/
makepkg -si --noconfirm

## zsh install and config
cd ~
sudo pacman -S zsh zsh-syntax-highlighting autojump zsh-autosuggestions --noconfirm
wget https://github.com/ChrisTitusTech/zsh/raw/master/.zshrc -O ~/.zshrc
mkdir -p "$HOME/.zsh"
wget https://github.com/ChrisTitusTech/zsh/raw/master/.zsh/aliasrc -O ~/.zsh/aliasrc
git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
touch ~/.cache/zshhistory
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>! ~/.zshrc
paru -S awesome-terminal-fonts --noconfirm

## theme
wget -qO- https://git.io/papirus-icon-theme-install | sh

## apps
sudo pacman -S termite terminator python gtop newsboat cmatrix nemo gedit geany flatpak appimagelauncher --noconfirm
sudo pip install clockr
paru -S signal-desktop --noconfirm
paru -S brave-bin--noconfirm
paru -S barrier --noconfirm
paru -S libreoffice-fresh --noconfirm
paru -S lightcord-bin --noconfirm
paru -S lutris --noconfirm
paru -S steam --noconfirm
paru -S lightdm-webkit2-greeter lightdm-webkit-theme-aether --noconfirm
paru -S freetube-bin --noconfirm
paru -S rhythmbox --noconfirm
paru -S joplin-desktop --noconfirm
paru -S virt-manager --noconfirm
paru -S ice-ssb --noconfirm
paru -S hypnotix --noconfirm
paru -S celluloid --noconfrim
paru -S obs-studio --noconfirm
paru -S kdenlive --noconfrim
paru -S devede --noconfirm
paru -S gimp --noconfirm
paru -S corectrl --noconfirm
paru -S grub-customizer --noconfirm
paru -S keepassxc --noconfirm
paru -S scrcpy --noconfirm
paru -S bleachbit --noconfirm
paru -S figlet lolcat --noconfirm 
paru -S nextcloud-client --noconfirm
paru -S qt5-styleplugins --noconfirm
paru -S nemo-fileroller --noconfirm
paru -S flameshot --noconfirm
paru -S timeshift-bin --noconfirm
paru -S gnome-disk-utility --noconfirm
flatpak install flathub org.gnome.DajaDup -y
paru -S gvfs-smb sshfs --noconfirm
echo 'RADV_PERFTEST=aco' | sudo tee -a /etc/environment

echo 'vm.vm.swappiness = 10
vm.vfs_cache_pressure = 50
vm.watermark_scale_factor = 200
vm.dirty_ratio = 3
' \ sudo tee -a /etc/sysctl.conf

## game client install
paru -S wine lutris steam --noconfirm

## gamemode install
mkdir ~/Downloads/git
paru -S meson systemd git dbus --noconfirm
cd ~/Downloads/git
git clone https://github.com/FeralInteractive/gamemode.git
cd gamemode
git checkout 1.5.1 # omit to build the master branch
./bootstrap.sh

## vkbasalt
cd ~/Downloads/git
git clone https://github.com/DadSchoorse/vkBasalt.git
cd vkBasalt
cd config
echo "add (smaa:smaa:cas) to line 10"
sleep 10s
nano vkBasalt.conf
cd ..
meson --buildtype=release --prefix=/usr builddir
ninja -C builddir install

## vim config 
paru -S vim vim-plug
cd ~
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
wget https://raw.githubusercontent.com/ChrisTitusTech/myvim/master/.vimrc

## clone termite config
mkdir ~/.config/termite/
git clone https://gitlab.com/batvin123/my-termite-config.git ~/.config/termite

## backgrounds
cd ~/Pictures
git clone https://gitlab.com/dwt1/wallpapers.git

## final update and clear install cache
paru -Syyuu --noconfirm
paru -Scc --noconfirm
clear
echo
echo
echo "REBOOT your pc"

